const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const swaggerUi = require("swagger-ui-express");
const cors = require("cors");
const cookieSession = require("cookie-session");
const passportConfig = require("./config/passport");
const passport = require("passport");
const dotenv = require("dotenv").config();
const routes = require("./routes/index");
const keys = require("./config/keys");
const errorHandler = require("./helpers/errors");
const app = express();
const swaggerDocument = require("./swagger.json");
const morgan = require("morgan");

mongoose
  .connect(process.env.MONGODB_URI, { useNewUrlParser: true })
  .then(result => {
    // Listen on a port
    const port = process.env.PORT || 2000;
    app.listen(port, err => {
      if (err) {
        console.log(`Error while starting the server on port ${port}`);
      } else {
        console.log(`Server connected on port ${port} & MongoDB is connected`);
      }
    });
  })
  .catch(err => console.log(err));

mongoose.set("useNewUrlParser", true);
mongoose.set("useFindAndModify", false);
mongoose.set("useCreateIndex", true);

app.use(require("express-status-monitor")());

// Body parser middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Session cookies
app.use(
  cookieSession({
    maxAge: 24 * 60 * 60 * 1000,
    keys: [keys.session.cookieKey]
  })
);

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// CORS
app.use(cors());

// Error
app.use(errorHandler.logErrors);
app.use(errorHandler.clientErrorHandler);
app.use(errorHandler.errorHandler);

// Morgan
app.use(
  morgan(
    ":date[web] | :method :url :status :res[content-length] - :response-time ms"
  )
);

// Basic home route
app.get("/api/v1", (req, res) => {
  res.json("Foodie API Version 1.0.0");
});

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// Use routes
app.use("/api/v1", routes);
