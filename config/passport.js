const passport = require("passport");
const keys = require("./keys");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const InstagramStrategy = require("passport-instagram").Strategy;
const User = require("../models/User");

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
  try {
    const user = await User.findById(id);
    if (user) done(null, user);
  } catch (err) {
    console.log(err);
  }
});

passport.use(
  new GoogleStrategy(
    {
      clientID: keys.google.client_id,
      clientSecret: keys.google.client_secret,
      callbackURL: "/api/v1/auth/google/callback"
    },
    async (accessToken, refreshToken, profile, done) => {
      try {
        const user = await User.findOne({ googleId: profile.id });
        if (user) {
          done(null, user);
        } else {
          const user = new User({
            googleId: profile.id,
            username: profile.displayName,
            email: profile.emails[0].value
          });
          const newUser = await user.save();
          if (newUser) {
            console.log(`New user created ${newUser}`);
            done(null, newUser);
          }
        }
      } catch (err) {
        console.log(err);
      }
    }
  )
);

passport.use(
  new InstagramStrategy(
    {
      clientID: keys.instagram.client_id,
      clientSecret: keys.instagram.client_secret,
      callbackURL: "/api/v1/auth/instagram/callback"
    },
    async (accessToken, refreshToken, profile, done) => {
      try {
        const user = await User.findOne({ instagramId: profile.id });
        if (user) {
          done(null, user);
        } else {
          const user = new User({
            instagramId: profile.id,
            username: profile.displayName
          });
          const newUser = await user.save();
          if (newUser) {
            console.log(`New user created ${newUser}`);
            done(null, newUser);
          }
        }
      } catch (err) {
        console.log(err);
      }
    }
  )
);
