const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const followersSchema = new Schema(
  {
    follower: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    },
    followee: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Followers", followersSchema);
