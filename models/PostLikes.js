const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const postLikesSchema = new Schema(
  {
    post: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Post"
    },
    liker: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Likes", postLikesSchema)
