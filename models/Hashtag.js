const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const hashtagSchema = new Schema({
  name: {
    type: String,
    unique: true
  },
  follower: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "User",
    default: []
  },
  posts: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "Posts",
    default: []
  }
});

hashtagSchema.index({ name: "text" });
module.exports = mongoose.model("Hashtag", hashtagSchema);
