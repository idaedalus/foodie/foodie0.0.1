const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const postSchema = new Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    },
    contentUrl: {
      type: [String],
      required: true
    },
    description: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Description"
    },
    likes: {
      type: Number,
      default: 0
    },
    liked_by: {
      type: [mongoose.Schema.Types.ObjectId],
      ref: "PostLikes"
    },
    comments: {
      type: [mongoose.Schema.Types.ObjectId],
      ref: "Comment"
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Post", postSchema);
