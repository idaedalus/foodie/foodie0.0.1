const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const descriptionSchema = new Schema(
  {
    caption: {
      type: String,
      maxlength: 140,
      required: true
    },
    content: {
      type: String,
      maxlength: 2200,
      required: true
    },
    recipe: {
      ingredients: String,
      difficulty: String,
      cooking_time: Number,
      typeOfRecipe: String,
      typeOfFood: String
    },
    restaurant: {
      restaurant_name: String,
      rating: {
        value: Number,
        description: {
          type: String,
          maxlength: 600
        }
      },
      price: String,
      isValet: Boolean,
      isBar: Boolean,
      location: String,
      typeOfDish: String,
      typeOfRestaurant: String,
      bookingRequired: String
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Description", descriptionSchema);
