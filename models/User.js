const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    username: {
      type: String
    },
    firstName: {
      type: String
    },
    lastName: {
      type: String
    },
    dob: {
      type: String
    },
    website: {
      type: String
    },
    gender: {
      type: String
    },
    bio: {
      type: String
    },
    picture: {
      type: String
    },
    email: {
      type: String
    },
    googleId: {
      type: String
    },
    instagramId: {
      type: String
    },
    mobile: {
      type: Number
    },
    following: {
      type: Array
    },
    followers: {
      type: Array
    },
    password: {
      type: String
    },
    posts: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Post"
      }
    ],
    secretToken: {
      type: String
    },
    accessToken: {
      type: String,
      required: true
    },
    verified: {
      type: Boolean,
      default: false
    },
    likes: {
      type: Number,
      ref: "PostLikes"
    },
    resetPasswordToken: String,
    resetPasswordExpires: String
  },
  { timestamps: true }
);

module.exports = mongoose.model("User", userSchema);
