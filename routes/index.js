const express = require("express");
const passport = require("passport");
const multer = require("multer");
const upload = multer();
const verifyToken = require("../helpers/verifyToken");
const authController = require("../controllers/auth");
const profileController = require("../controllers/profile");
const postController = require("../controllers/post");
const followController = require("../controllers/follow");
const postLikesController = require("../controllers/likes");
const hashtagsController = require("../controllers/hashtags");
const commentsController = require("../controllers/comment");
const router = express.Router();

// Authorization routes
router.route("/auth/signup").post(upload.none(), authController.signup);
router.route("/auth/signin").post(upload.none(), authController.signin);
router.route("/auth/google").get(
  passport.authenticate("google", {
    scope: ["profile", "email"]
  })
);
router.route("/auth/instagram").get(passport.authenticate("instagram"));
router
  .route("/auth/instagram/callback")
  .get(passport.authenticate("instagram"), authController.instagramRedirect);
router
  .route("/auth/google/callback")
  .get(passport.authenticate("google"), authController.googleRedirect);

router.route("/auth/verify/:secretToken").get(authController.verifyEmail);
router.route("/auth/forgot-password").post(authController.forgotPassword);
router.route("/auth/reset/:token").get(authController.resetPassword);
router.route("/auth/reset/:token").post(authController.resetPasswordPost);

// Profile routes
router
  .route("/profile")
  .post(
    verifyToken,
    upload.single("picture"),
    profileController.verifyUserByAccessToken,
    profileController.updateProfile
  );

router
  .route("/profile")
  .get(
    verifyToken,
    upload.none(),
    profileController.verifyUserByAccessToken,
    profileController.getProfile
  );

router.route("/search").get(upload.none(), profileController.searchProfile);

// Post routes
router
  .route("/post/upload")
  .post(
    verifyToken,
    upload.array("media", 8),
    profileController.verifyUserByAccessToken,
    postController.uploadMedia
  );
router
  .route("/post")
  .post(
    verifyToken,
    upload.array("media", 8),
    profileController.verifyUserByAccessToken,
    postController.createPost
  );

router
  .route("/posts")
  .get(
    verifyToken,
    upload.none(),
    profileController.verifyUserByAccessToken,
    postController.getPosts
  );

router
  .route("/post")
  .delete(
    verifyToken,
    upload.none(),
    profileController.verifyUserByAccessToken,
    postController.deletePost
  );

router
  .route("/post/edit")
  .put(
    verifyToken,
    upload.array("media", 8),
    profileController.verifyUserByAccessToken,
    postController.editPost
  );

router.route("/post/").get(upload.none(), postController.sharePost);

// Follow & Unfollow
router
  .route("/follow")
  .post(
    verifyToken,
    upload.none(),
    profileController.verifyUserByAccessToken,
    followController.follow
  );

router
  .route("/unfollow")
  .post(
    verifyToken,
    upload.none(),
    profileController.verifyUserByAccessToken,
    followController.unfollow
  );

// Like a post
router
  .route("/like")
  .get(
    verifyToken,
    upload.none(),
    profileController.verifyUserByAccessToken,
    postLikesController.like
  );

// Hashtags
router.route("/hashtag").post(upload.none(), hashtagsController.createHashtag);
router
  .route("/hashtag/search")
  .get(upload.none(), hashtagsController.searchHashtag);

// Comments
router
  .route("/comment")
  .post(
    verifyToken,
    upload.none(),
    profileController.verifyUserByAccessToken,
    commentsController.comment
  );
module.exports = router;
