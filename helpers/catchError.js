module.exports = (response, status, object) => {
  return response.status(status).json(object);
};
