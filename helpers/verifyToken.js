const jwt = require("jsonwebtoken");

function verifyToken(req, res, next) {
  const bearerHeader = req.headers["authorization"];
  // console.log(req.headers["authorization"])
  if (typeof bearerHeader !== "undefined") {
    const bearer = bearerHeader.split(" ");
    const bearerToken = bearer[1] || bearer[0];
    jwt.verify(bearerToken, "somegreatkey", (err, decoded) => {
      if (err) {
        console.log(err)
        res.status(401).json(err);
      } else {
        next();
      }
    });
  } else {
    // Forbidden
    res.sendStatus(403);
  }
}

module.exports = verifyToken;
