const mailer = require("nodemailer");
const dotenv = require("dotenv").config();

exports.verifyEmail = async (userMail, secretToken = null, purpose) => {
  console.log(userMail);
  let transporter = mailer.createTransport({
    host: "us2.smtp.mailhostbox.com",
    port: 587,
    auth: {
      user: process.env.EMAIL,
      pass: process.env.PASSWORD
    }
  });
  let mailOptions = {};
  let serverUrl =
    process.env.SERVER || "http://localhost:2000/api/v1/auth/verify";
  if (purpose.id === 1) {
    mailOptions = {
      from: `Qripsy Support ${process.env.EMAIL}`, // sender address
      to: userMail, // list of receivers
      subject: "Welcome to Qripsy", // Subject line
      html: `Verify your account: ${serverUrl}/${secretToken}` // html body
    };
  }

  if (purpose.id === 2) {
    mailOptions = {
      from: `Qripsy Support ${process.env.EMAIL}`, // sender address
      to: userMail, // list of receivers
      subject: "Forgot Password", // Subject line
      html:
        "You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n" +
        "Please click on the following link, or paste this into your browser to complete the process:\n\n" +
        "https://" +
        purpose.host +
        "/auth/reset/" +
        purpose.token +
        "\n\n" +
        "If you did not request this, please ignore this email and your password will remain unchanged.\n"
      // html body
    };
  }

  if (purpose.id === 3) {
    mailOptions = {
      from: `Qripsy Support ${process.env.EMAIL}`,
      to: userMail,
      subject: "Your password has been changed",
      html:
        "Hello,\n\n" +
        "This is a confirmation that the password for your account " +
        userMail +
        " has just been changed.\n"
    };
  }
  console.log(mailOptions);
  let info = await transporter.sendMail(mailOptions);
  console.log(`Email sent ${info.messageId}`);
  if (info.messageId) {
    return true;
  }
};
