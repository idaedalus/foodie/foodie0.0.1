const User = require("../models/User");
const Post = require("../models/Post");
const PostLikes = require("../models/PostLikes");

// Like a post
exports.like = async (req, res, next) => {
  const userID = req.query.userID;
  const postId = req.query.postId;

  if (postId && userID) {
    try {
      const postLikes = await PostLikes.findOne({
        post: postId,
        liker: userID
      });

      // Check if the post is already liked
      if (!postLikes) {
        const post = await Post.findByIdAndUpdate(
          { _id: postId },
          {
            $inc: {
              likes: 1
            },
            $push: {
              liked_by: userID
            }
          },
          { new: true }
        );

        if (post) {
          const postLike = new PostLikes({ post: postId, liker: userID });
          const savedPostLike = await postLike.save();
          if (savedPostLike) {
            res.status(200).json({ message: "Post liked", data: post });
          }
        }
      } else {
        // If the post is already liked, then dislike the post
        const post = await Post.findByIdAndUpdate(
          { _id: postId },
          {
            $inc: {
              likes: -1
            },
            $pull: {
              liked_by: userID
            }
          },
          { new: true }
        );

        if (post) {
          const savedPostLike = await PostLikes.deleteOne({
            post: postId,
            liker: userID
          });
          if (savedPostLike) {
            res.status(200).json({ message: "Post disliked", data: post });
          }
        }
      }
    } catch (err) {
      next(err);
    }
  } else {
    res.status(200).json({ message: "Query Parameters not found" });
  }
};
