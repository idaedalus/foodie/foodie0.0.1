const Hashtag = require("../models/Hashtag");

// Get the hashtag as a qrispy profile
exports.getHashtagProfile = async (req, res, next) => {
  const hashtag = req.query.name;

  try {
    const hashtagFromDb = await Hashtag.findOne({ name: hashtag })
      .populate("follower")
      .populate("posts")
      .exec();
    if (hashtagFromDb) {
      res.status(200).json(hashtagFromDb);
    } else {
      res.status(200).json({ message: "Hashtag not found" });
    }
  } catch (err) {
    next(err);
  }
};

// Create a hashtag
exports.createHashtag = async (req, res, next) => {
  const hashtag = req.query.name;

  try {
    const newHashtag = new Hashtag({ name: hashtag });
    if (newHashtag) {
      const saveHashtag = await newHashtag.save();
      if (saveHashtag) {
        res
          .status(200)
          .json({ message: "Hashtag created", hashtag: saveHashtag });
      } else {
        res.status(500).json({ message: "failed to create a hashtag" });
      }
    } else {
      res.status(500).json({ message: "could not create hashtag object" });
    }
  } catch (err) {
    next(err);
  }
};

// Search for a hashtag
exports.searchHashtag = async (req, res, next) => {
  let name = req.query.name;

  if (name) {
    try {
      //   const tag = await Hashtag.find(
      //     { $text: { $search: name } },
      //     { score: { $meta: "textScore" } }
      //   ).sort({ score: { $meta: "textScore" } });

      // Find the hashtag by matching the first three letters
      const tag = await Hashtag.find({
        name: {
          $regex: new RegExp(name.substring(0, 3))
        }
      });
      if (tag) {
        res.status(200).json(tag);
      }
    } catch (err) {
      next(err);
    }
  } else {
    res.status(200).json({ message: "Hashtag not found in query" });
  }
};
