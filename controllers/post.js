const User = require("../models/User");
const Post = require("../models/Post");
const Followers = require("../models/Followers");
const mongoose = require("mongoose");
const Description = require("../models/Description");
const multerFunctions = require("../helpers/multer");

// Create a post
exports.createPost = async (req, res, next) => {
  const userID = req.query.userID;
  const postType = req.query.post_type;
  let urlArray = [],
    promises = [];

  if (userID) {
    // console.log(typeof req.files[0]);
    console.log(req.files);
    console.log(req.body);
    if (req.files.length !== 0) {
      // console.log(req.files[0]);
      req.files.forEach(async file => {
        promises.push(multerFunctions.uploadImageToStorage(file));
      });

      // Upload all images and videos
      urlArray = await Promise.all(promises);
    }
    // Check if its a recipe
    if (postType === "recipe") {
      try {
        const description = new Description({
          caption: req.body.caption,
          content: req.body.content,
          recipe: {
            typeOfRecipe: req.body.typeOfRecipe,
            ingredients: req.body.ingredients,
            difficulty: req.body.difficulty,
            cooking_time: req.body.cooking_time,
            typeOfFood: req.body.typeOfFood
          }
        });
        const saveDescriptionPost = await description.save();
        if (saveDescriptionPost) {
          const recipePost = new Post({
            user: userID,
            contentUrl: urlArray,
            description: saveDescriptionPost._id
          });
          const saveRecipePost = await recipePost.save();
          if (saveRecipePost) {
            const user = await User.findOneAndUpdate(
              { _id: userID },
              {
                $push: { posts: saveRecipePost._id }
              },
              { new: true }
            );
            //   console.log(user);
            if (user) {
              res.status(201).json({
                message: "Recipe Post created",
                postId: saveRecipePost._id
              });
            }
          }
        }
      } catch (err) {
        next(err);
      }
      // Check if its a review
    } else if (postType === "review") {
      try {
        const description = new Description({
          caption: req.body.caption,
          content: req.body.content,
          restaurant: {
            restaurant_name: req.body.restaurant_name,
            rating: {
              value: req.body.ratingValue,
              description: req.body.ratingDescription
            },
            price: req.body.price,
            isValet: req.body.isValet,
            isBar: req.body.isBar,
            location: req.body.location,
            typeOfDish: req.body.typeOfDish,
            typeOfRestaurant: req.body.typeOfRestaurant,
            bookingRequired: req.body.bookingRequired
          }
        });
        const saveDescriptionPost = await description.save();
        if (saveDescriptionPost) {
          const restaurantPost = new Post({
            user: userID,
            contentUrl: urlArray,
            description: saveDescriptionPost._id
          });
          const saveRestaurantPost = await restaurantPost.save();
          if (saveRestaurantPost) {
            const user = await User.findOneAndUpdate(
              { _id: userID },
              {
                $push: { posts: saveRestaurantPost._id }
              },
              { new: true }
            );
            //   console.log(user);
            if (user) {
              res.status(201).json({ message: "Restaurant Post created" });
            }
          }
        }
      } catch (err) {
        next(err);
      }
    }
  } else {
    res.status(500).json({ message: "CREATEPOST: userID not provided" });
  }
};

// Get all posts
exports.getPosts = async (req, res, next) => {
  const username = req.query.username;
  const limit = parseInt(req.query.limit, 10) || 10;
  const page = parseInt(req.query.page, 10) || 0;
  let ids = [];

  try {
    const user = await User.findOne({ username: username }).select("id");
    const following = await Followers.find({ follower: user });
    following.forEach(id => {
      ids.push(mongoose.Types.ObjectId(id.followee));
    });

    const posts = await Post.find({
      user: {
        $in: ids
      }
    })
      .populate("user", "username picture _id")
      .populate("description")
      .sort({ createdAt: -1 })
      .skip(page * limit)
      .limit(limit)
      .exec();

    if (posts) {
      res.status(200).json(posts);
    }
  } catch (err) {
    next(err);
  }
};

// Delete a post by it's id
exports.deletePost = async (req, res, next) => {
  const postId = req.query.postId;

  try {
    const deletedPost = await Post.findById(postId);
    const deletePostDescId = deletedPost.description;
    const deletedPostUserId = deletedPost.user;

    const postDeleted = await Post.deleteOne({ _id: postId });
    if (postDeleted) {
      const descDeleted = await Description.deleteOne({
        _id: deletePostDescId
      });
      if (descDeleted) {
        const userPostDecrement = await User.findByIdAndUpdate(
          { _id: deletedPostUserId },
          {
            $pull: {
              posts: postId
            }
          }
        );
        if (userPostDecrement) {
          res
            .status(200)
            .json({ message: "Post Deleted", deletedPostId: postId });
        }
      }
    }
  } catch (err) {
    next(err);
  }
};

// Edit by post
exports.editPost = async (req, res, next) => {
  // if (err) next(err);
  const postId = req.query.postId;
  const postType = req.query.postType;
  let urlArray = [];
  let promises = [];
  let editPost = {};

  const post = await Post.findById(postId);
  if (!post) {
    res.status(200).json({ message: "Post not found" });
    return;
  }

  if (postId) {
    if (req.files.length > 0) {
      req.files.forEach(async file => {
        promises.push(multerFunctions.uploadImageToStorage(file));
      });
    }

    urlArray = await Promise.all(promises);
    // console.log(urlArray);

    const postUrlUpdate = await Post.findByIdAndUpdate(
      { _id: postId },
      {
        $set: {
          contentUrl: urlArray
        }
      },
      { new: true }
    );
    // console.log(postUrlUpdate);

    // Caption, content, ingredients, restaurant name & location can only be edited
    if (req.body.caption) {
      editPost.caption = req.body.caption;
    }

    if (req.body.content) {
      editPost.content = req.body.content;
    }

    if (postType === "recipe") {
      if (req.body.ingredients) {
        editPost.ingredients = req.body.ingredients;
      }

      try {
        const descId = post.description;
        const descUpdate = await Description.findByIdAndUpdate(
          { _id: descId },
          {
            $set: {
              content: editPost.content,
              caption: editPost.caption,
              recipe: {
                ingredients: editPost.ingredients
              }
            }
          },
          { new: true }
        );

        if (descUpdate) {
          res
            .status(200)
            .json({ message: "Recipe post updated.", postId: postId });
        }
      } catch (err) {
        next(err);
      }
    } else if (postType === "review") {
      if (req.body.restaurant_name) {
        editPost.restaurant_name = req.body.restaurant_name;
      }

      if (req.body.location) {
        editPost.location = req.body.location;
      }

      try {
        const post = await Post.findById(postId);
        const descId = post.description;

        const descUpdate = await Description.findByIdAndUpdate(
          { _id: descId },
          {
            $set: {
              content: editPost.content,
              caption: editPost.caption,
              restaurant: {
                restaurant_name: editPost.restaurant_name,
                location: editPost.location
              }
            }
          },
          { new: true }
        );

        console.log(descUpdate);
        if (descUpdate) {
          res
            .status(200)
            .json({ message: "Restaurant post updated.", postId: postId });
        }
      } catch (err) {
        next(err);
      }
    }
  }
};

// Share a post
exports.sharePost = async (req, res, next) => {
  const postId = req.query.postId;
  if (postId) {
    try {
      const post = await Post.findById(postId)
        .populate("user", "username picture")
        .populate("description")
        .exec();

      if (post) {
        res.status(200).json(post);
      } else {
        res.status(200).json({ message: "Post not found" });
      }
    } catch (err) {
      next(err);
    }
  } else {
    res.status(200).json({ message: "Post ID unavailable" });
  }
};

exports.uploadMedia = async (req, res, next) => {
  let urlArray = [],
    promises = [];
  if (req.files.length === 0) {
    res.status(200).json({ message: "Minimum one image/video is required" });
  }

  req.files.forEach(async file => {
    promises.push(multerFunctions.uploadImageToStorage(file));
  });

  urlArray = await Promise.all(promises);
  if (urlArray) {
    res.status(200).json(urlArray);
  }
};
