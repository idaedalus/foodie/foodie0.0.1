const Followers = require("../models/Followers");
const User = require("../models/User");

// Follow a user
exports.follow = async (req, res, next) => {
  const baseUser = req.query.follower;
  const user = req.query.followee;

  console.log(baseUser, user);

  try {
    if (baseUser && user) {
      // Get ids of both the users
      const userToBeFollowedId = await User.findOne({ username: user }).select(
        "_id"
      );
      const baseUserId = await User.findOne({ username: baseUser }).select(
        "_id"
      );

      console.log(baseUserId);
      console.log(userToBeFollowedId);

      // Get the document of the user to be followed from the Followers collection
      const userToBeFollowed = await Followers.findOne({
        follower: baseUserId,
        followee: userToBeFollowedId
      });
      console.log(userToBeFollowed);

      // Check if that user already follows, if not save the user
      if (!userToBeFollowed) {
        const newFollower = new Followers({
          followee: userToBeFollowedId,
          follower: baseUserId
        });
        console.log("i am here 2");
        const follow = await newFollower.save();
        if (follow) {
          // Update the parent user objects by updating the followers and following arrays
          const userToBeFollowedUpdate = await User.findOneAndUpdate(
            { _id: userToBeFollowedId },
            {
              $push: { followers: baseUserId }
            },
            { new: true }
          );
          const userToBeFollowedUpdate2 = await User.findOneAndUpdate(
            { _id: baseUserId },
            {
              $push: { following: userToBeFollowedId }
            },
            { new: true }
          );

          if (userToBeFollowedUpdate && userToBeFollowedUpdate2) {
            console.log(userToBeFollowedUpdate);
            console.log(userToBeFollowedUpdate2);
            res.status(200).json({ message: "Followed" });
          }
        }
      } else {
        res.status(200).json({ message: "Already following" });
      }
    } else {
      res.status(200).json({ message: "No username given" });
    }
  } catch (err) {
    next(err);
  }
};

// Unfollow a user
exports.unfollow = async (req, res, next) => {
  const baseUser = req.query.follower;
  const user = req.query.followee;

  try {
    if (baseUser && user) {
      // Get the user ids
      const userToBeUnFollowedId = await User.findOne({
        username: user
      }).select("_id");
      const baseUserId = await User.findOne({ username: baseUser }).select(
        "_id"
      );

      // Remove the user to be unfollowed from the Followers collection
      const userToBeUnFollowed = await Followers.deleteOne({
        follower: baseUserId,
        followee: userToBeUnFollowedId
      });

      if (userToBeUnFollowed) {
        // Update the following and followers arrays in the parent user's object
        const userToBeUnFollowedUpdate = await User.findOneAndUpdate(
          { _id: userToBeUnFollowedId },
          {
            $pull: { followers: baseUserId._id }
          },
          { new: true }
        );
        const userToBeUnFollowedUpdate2 = await User.findOneAndUpdate(
          { _id: baseUserId },
          {
            $pull: { following: userToBeUnFollowedId._id }
          },
          { new: true }
        );

        if (userToBeUnFollowedUpdate && userToBeUnFollowedUpdate2) {
          console.log(userToBeUnFollowedUpdate);
          console.log(userToBeUnFollowedUpdate2);
          res.status(200).json({ message: "Unfollowed" });
        }
      } else {
        res.status(200).json({ message: "The user does not follow" });
      }
    }
  } catch (err) {
    next(err);
  }
};
