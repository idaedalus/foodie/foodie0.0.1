const validators = require("../helpers/validators");
const mongoose = require("mongoose");
const User = require("../models/User");
const Post = require("../models/Post");
const multerFunctions = require("../helpers/multer");

// Update the profile
exports.updateProfile = async (req, res, next) => {
  const { email, firstName, lastName, dob, gender } = req.body;
  const website = req.body.website || "";
  const bio = req.body.bio || "";

  const picture = req.file;

  if (picture) {
    let imageUrl = "";
    try {
      if (validators.isEmail(email)) {
        imageUrl = await multerFunctions.uploadImageToStorage(picture);
        console.log(imageUrl);
        const user = await User.findOneAndUpdate(
          { email: email },
          {
            firstName: firstName,
            lastName: lastName,
            dob: dob,
            gender: gender,
            website: website,
            bio: bio,
            picture: imageUrl
          },
          { new: true }
        );
        if (user) {
          res.status(200).json({
            message: "User updated",
            user: user
          });
        }
      }
    } catch (err) {
      next(err);
    }
  } else {
    try {
      if (validators.isEmail(email)) {
        const user = await User.findOneAndUpdate(
          { email: email },
          {
            firstName: firstName,
            lastName: lastName,
            dob: dob,
            gender: gender,
            website: website,
            bio: bio
          },
          { new: true }
        );
        if (user) {
          res.status(200).json({
            message: "User updated",
            user: {
              firtName: user.firstName,
              lastName: user.lastName,
              username: user.username,
              gender: user.gender,
              email: user.email,
              bio: user.bio,
              picture: user.picture,
              website: user.website
            }
          });
        }
      }
    } catch (err) {
      next(err);
    }
  }
};

// Get a the profile of the current logged in user
exports.getProfile = async (req, res, next) => {
  const limit = parseInt(req.query.limit, 10) || 10;
  const page = parseInt(req.query.page, 10) || 0;
  const email = req.query.email;

  try {
    const userProfile = await User.findOne({ email: email });
    if (userProfile) {
      const posts = await Post.find({
        _id: {
          $in: userProfile.posts
        }
      })
        .populate("user", "username picture")
        .populate("description")
        .sort({ createdAt: -1 })
        .skip(page * limit)
        .limit(limit)
        .exec();

      if (posts.length > 0) {
        userProfile.posts = posts;
        const postsLiked = await Post.aggregate([
          {
            $match: { user: userProfile._id }
          },
          { $group: { _id: null, totalLikes: { $sum: "$likes" } } }
        ]);
        userProfile["likes"] = postsLiked[0].totalLikes;
      } else {
        userProfile["likes"] = 0;
      }
      res.status(200).json(userProfile);
    }
  } catch (err) {
    next(err);
  }
};

// Search for a user profile
exports.searchProfile = async (req, res, next) => {
  const username = req.query.username;
  const limit = parseInt(req.query.limit, 10) || 10;
  const page = parseInt(req.query.page, 10) || 0;

  try {
    if (username) {
      const userProfileFetched = await User.findOne({
        username: username
      });
      if (userProfileFetched) {
        // console.log(userProfileFetched)
        let postIds = [];
        userProfileFetched.posts.forEach(id => {
          postIds.push(mongoose.Types.ObjectId(id));
        });

        let postsFetched = await Post.find({
          _id: {
            $in: postIds
          }
        })
          .populate("user", "_id username picture")
          .populate("description")
          .sort({ createdAt: -1 })
          .skip(page * limit)
          .limit(limit);

        let searchedUser = {
          userID: userProfileFetched._id,
          firstName: userProfileFetched.firstName,
          lastName: userProfileFetched.lastName,
          username: userProfileFetched.username,
          bio: userProfileFetched.bio,
          website: userProfileFetched.website,
          followers: userProfileFetched.followers,
          following: userProfileFetched.following,
          posts: postsFetched,
          picture: userProfileFetched.picture
        };

        if (postsFetched.length > 0) {
          userProfileFetched.posts = postsFetched;
          const postsLiked = await Post.aggregate([
            {
              $match: { user: userProfileFetched._id }
            },
            { $group: { _id: null, totalLikes: { $sum: "$likes" } } }
          ]);
          searchedUser["likes"] = postsLiked[0].totalLikes;
        } else {
          searchedUser["likes"] = 0;
        }

        res.status(200).json(searchedUser);
      } else {
        res.status(200).json({ message: "User not found" });
      }
    }
  } catch (err) {
    next(err);
  }
};

// Verify a user by it's access token
exports.verifyUserByAccessToken = async (req, res, next) => {
  const access_token = req.headers.access_token;
  // console.log(req.headers);
  console.log("Access Token: ", access_token);
  if (access_token) {
    try {
      const user = await User.findOne({ accessToken: access_token });
      if (user) {
        next();
      } else {
        res.status(200).json({ message: "Access Token Error" });
      }
    } catch (err) {
      next(err);
    }
  } else {
    res.status(200).json({ message: "Access Token Error" });
  }
};
