const User = require("../models/User");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const crypto = require("crypto");
const email = require("../helpers/email").verifyEmail;
const dotenv = require("dotenv").config();
const randomStringGenerator = require("randomstring");
const cryptoRandomString = require("crypto-random-string");
const validators = require("../helpers/validators");
const catchError = require("../helpers/catchError");

// Sign up to the web app using email and password
exports.signup = async (req, res, next) => {
  const {
    emailOrMobile,
    password,
    username,
    firstName,
    lastName,
    dob,
    gender
  } = req.body;
  let user, hashedPassword;
  try {
    if (emailOrMobile) {
      // console.log("i am here");
      if (
        validators.isEmail(emailOrMobile) &&
        password &&
        username &&
        lastName &&
        dob &&
        gender
      ) {
        const userAlreadyExists = await User.findOne({ email: emailOrMobile });
        if (userAlreadyExists) {
          res.status(200).json({ message: "User already exists" });
          return;
        }

        // Generate a secret token for email verification
        const secretToken = randomStringGenerator.generate();
        const accessToken = cryptoRandomString({ length: 15 });
        console.log(secretToken);
        console.log(accessToken);

        // Create a transporter to send emails via qrispy's mail server
        const isSent = await email(emailOrMobile, secretToken, 1);
        if (isSent) {
          // Create a hashed password and save the user to the database
          hashedPassword = await bcrypt.hash(password, 12);
          user = new User({
            firstName: firstName,
            lastName: lastName,
            username: username,
            dob: dob,
            gender: gender,
            email: emailOrMobile,
            password: hashedPassword,
            secretToken: secretToken,
            accessToken: accessToken
          });
          user = await user.save();
          res.status(201).json({
            message: "User created",
            userID: user.userID
          });
        } else {
          res.status(200).json({ message: "Failed to send email" });
        }
      } else {
        res.status(200).json({ message: "Something is missing." });
      }
    }
  } catch (err) {
    next(err);
  }
};

// Google social login for redirect
exports.googleRedirect = (req, res, next) => {
  res.send("authenticated");
};

// Instagram social login for redirect
exports.instagramRedirect = (req, res, next) => {
  res.send("authenticated");
};

// Email verification
exports.verifyEmail = async (req, res, next) => {
  const secretToken = req.params.secretToken;
  try {
    const user = await User.findOne({ secretToken: secretToken });
    if (!user) {
      res.status(403).json({
        message: "User not signed up. Verification failed"
      });
    } else {
      if (user.secretToken === secretToken) {
        user.verified = true;
        const verified = await user.save();
        if (verified) {
          // res.status(200).json({
          //   message: "User verified successfully."
          // });
          res.redirect(200, "https://foodie-website.herokuapp.com/");
        }
      }
    }
  } catch (err) {
    next(err);
  }
};

// Sign in to qrispy using email and password
exports.signin = async (req, res, next) => {
  const { email, password } = req.body;
  if (!email) {
    catchError(res, 403, { message: "Email not provided" });
    return;
  }

  try {
    const userFound = await User.findOne({ email });

    // Below is the validation check on the user object
    if (!userFound) {
      catchError(res, 404, { data: "User not found" });
      return;
    }

    const isPasswordEqual = await bcrypt.compare(password, userFound.password);
    if (!isPasswordEqual) {
      catchError(res, 403, { data: "Password entered is incorrect" });
      return;
    }

    const isUserVerified = userFound.verified;
    if (!isUserVerified) {
      catchError(res, 403, { data: "Email is not verified." });
      return;
    }

    // Create a jwt token
    const token = jwt.sign(
      { email: userFound.email, userId: userFound._id.toString() },
      "somegreatkey",
      { expiresIn: "365d" }
    );

    res.status(200).json({
      token: token,
      accessToken: userFound.accessToken,
      username: userFound.username,
      userID: userFound._id.toString()
    });
  } catch (err) {
    next(err);
  }
};

// Forgot password
exports.forgotPassword = async (req, res, next) => {
  const getEmail = req.query.email;
  const buffer = await crypto.randomBytes(20);
  if (!buffer) {
    next("/auth/forgot-password: Buffer could not be generated");
  }

  const token = buffer.toString("hex");
  const user = await User.findOne({ email: getEmail });
  if (!user) {
    res
      .status(200)
      .json({ message: "User with this email not found or does not exist" });
  }

  user.resetPasswordToken = token;
  user.resetPasswordExpires = Date.now() + 3600000;

  const saveUpdatedUser = await user.save();
  if (!saveUpdatedUser) {
    next(
      "/auth/forgot-password: User could not be updated with token and token expiry"
    );
  }

  const isSent = await email(getEmail, null, {
    id: 2,
    token: token,
    host: req.headers.host
  });
  if (isSent) {
    res
      .status(200)
      .json({ message: `An e-mail has been sent with further instructions.` });
  }
};

// Send token as a param for allowing the password to be changed
exports.resetPassword = async (req, res, next) => {
  const user = await User.findOne({
    resetPasswordToken: req.params.token,
    resetPasswordExpires: { $gt: Date.now() }
  });

  if (!user) {
    res
      .status(200)
      .json({ message: "Password reset token is invalid or has expired." });
  }

  res.status(200).json({ token: req.params.token });
};

// Change the password
exports.resetPasswordPost = async (req, res, next) => {
  const user = await User.findOne({
    resetPasswordToken: req.params.token,
    resetPasswordExpires: { $gt: Date.now() }
  });

  if (!user) {
    res
      .status(200)
      .json({ message: "Password reset token is invalid or has expired." });
  }

  user.password = req.body.password;
  user.resetPasswordToken = undefined;
  user.resetPasswordExpires = undefined;

  const saveUpdatedUser = await user.save();
  if (!saveUpdatedUser) {
    next("/auth/reset/:token - User could not be updated");
  }

  const isSent = await email(user.email, null, { id: 3 });
  if (isSent) {
    res
      .status(200)
      .json({ message: "Success! Your password has been changed." });
  }
};
