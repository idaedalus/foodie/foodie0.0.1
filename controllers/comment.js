const Comment = require("../models/Comment");
const Post = require("../models/Post");

// Comment on a post
exports.comment = async (req, res, next) => {
  let postId = "",
    comment = "",
    userId = "";
  if (req.query.userId && req.query.postId && req.body.comment) {
    postId = req.query.postId;
    comment = req.body.comment;
    userId = req.query.userId;

    const newComment = new Comment({
      userId: userId,
      postId: postId,
      comment: comment
    });
    let savedComment;
    if (comment) {
      try {
        savedComment = await newComment.save();
      } catch (err) {
        next(err);
      }
      if (savedComment) {
        try {
          const updatePost = await Post.findByIdAndUpdate(
            { _id: postId },
            {
              comments: { $push: savedComment._id }
            },
            { new: true }
          );
          if (updatePost) {
            res.status(200).json({
              message: "Comment added",
              commentId: newComment._id,
              postId: postId
            });
          } else {
            res.status(200).json({ message: "Post was not saved" });
          }
        } catch (err) {
          next(err);
        }
      } else {
        res.status(200).json({ message: "Err: Comment could not be saved" });
      }
    } else {
      res
        .status(200)
        .json({ message: "Err: Comment object could not be made" });
    }
  } else {
    res.status(200).json({ message: "PostId not found" });
  }
};

// Delete a comment
exports.deleteAComment = async (req, res, next) => {
  const commentId = req.query.commentId;

  if (commentId) {
    try {
      const deletedComment = await Comment.findByIdAndDelete(commentId);
      if (deletedComment) {
        res
          .status(200)
          .json({ message: "Comment was deleted", commentId: commentId });
      } else {
        res.status(200).json({ message: "Err while delteing the comment" });
      }
    } catch (err) {
      next(err);
    }
  } else {
    res
      .status(200)
      .json({ message: "Comment ID was not passed as a query parameter" });
  }
};
